﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WindowsStateHolder", menuName = "menugun/WindowsStateHolder")]
public class WindowsStateHolder : PersistentScriptableObject {
    [Serializable]
    public class WindowState {
        public string name;
        public Vector2 position;
        public Vector2 size;

        public WindowState(string name, Vector2 position, Vector2 size) {
            this.name = name;
            this.position = position;
            this.size = size;
        }
    }

	public List<WindowState> list = new List<WindowState>();

	public void SetWindowState(string name, Vector2 position, Vector2 size) {
        WindowState item = list.Find(x => x.name == name);
        if (item != null) {
            item.position = position;
            item.size = size;
        } else {
            list.Add(new WindowState(name, position, size));
        }
	}

	public WindowState GetWindowState(string name) {
        return list.Find(x => x.name == name);
	}
}
