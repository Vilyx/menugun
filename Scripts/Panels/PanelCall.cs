﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace MenuGun {
    public class PanelCall : Panel {

        public Button button;
        public Text text;

        public override void SetName(string name) {
            text.text = name;
        }

        public void Subscribe(Action action) {
            button.onClick.AddListener(() => action.Invoke());
        }

        public void Link(object o, MethodInfo mi) {
            GameObject go = Instantiate(gameObject, transform.parent);
            PanelCall pc = go.GetComponent<PanelCall>();

            pc.Identifier = mi.Name;

            pc.Subscribe(() => mi.Invoke(o, null));
        }
    }
}
