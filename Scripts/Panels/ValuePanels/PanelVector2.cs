﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MenuGun {
    public class PanelVector2 : ValuePanel {

        public InputField inputFieldX;
        public Slider sliderX;
        public InputField inputFieldY;
        public Slider sliderY;

        private Vector2 value;

        void Start() {
            // for some lucky reason this lines dont recurse each other
            inputFieldX.onValueChanged.AddListener((s) => {
                sliderX.value = float.Parse(s);
            });
            sliderX.onValueChanged.AddListener((f) => {
                inputFieldX.text = f.ToString();
            });

            inputFieldY.onValueChanged.AddListener((s) => {
                sliderY.value = float.Parse(s);
            });
            sliderY.onValueChanged.AddListener((f) => {
                inputFieldY.text = f.ToString();
            });
        }

        //void SetRange(float min, float max) {
        //    slider.minValue = min;
        //    slider.maxValue = max;
        //}

        void SetValue(Vector2 value) {
            this.value = value;
            sliderX.value = value.x;
            sliderY.value = value.y;
            inputFieldX.text = value.x.ToString();
            inputFieldY.text = value.y.ToString();
        }

        Vector2 GetValue() {
            return value;
        }

        public override bool CanHandle(Type t) {
            return t == typeof(Vector2);
        }

        public override void Subscribe(Action action) {
            inputFieldX.onValueChanged.AddListener(x => value.x = float.Parse(x));
            sliderX.onValueChanged.AddListener(x => value.x = x);

            inputFieldY.onValueChanged.AddListener(y => value.y = float.Parse(y));
            sliderY.onValueChanged.AddListener(y => value.y = y);


            inputFieldX.onValueChanged.AddListener(x => action.Invoke());
            sliderX.onValueChanged.AddListener(x => action.Invoke());

            inputFieldY.onValueChanged.AddListener(y => action.Invoke());
            sliderY.onValueChanged.AddListener(y => action.Invoke());
        }

        public override void AfterLink() {
            //MenuRangeAttribute ra = linker.customAttributes.FirstOrDefault(x => x is MenuRangeAttribute) as MenuRangeAttribute;
            //if (ra != null) {
            //    SetRange(ra.min, ra.max);
            //}

            Subscribe(() => linker.SetValue(GetValue()));
        }

        public override void Fetch() {
            SetValue((Vector2) linker.GetValue());
        }
    }
}
