﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace MenuGun {
    public class PanelBoolean : ValuePanel {

        public Toggle toggle;

        void SetValue(bool value) {
            toggle.isOn = value;
        }

        bool GetValue() {
            return toggle.isOn;
        }

        public override bool CanHandle(Type t) {
            return t == typeof(bool);
        }

        public override void Subscribe(Action action) {
            toggle.onValueChanged.AddListener((x) => action.Invoke());
        }

        public override void AfterLink() {
            Subscribe(() => linker.SetValue(GetValue()));
        }

        public override void Fetch() {
            SetValue((bool)linker.GetValue());
        }
    }
}
