﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
using MenuGun;
using System;

public class PersistentScriptableObject : ScriptableObject {

    protected string folder;
    protected string path;

    public string GetPath() {
        return path;
    }

    void OnEnable() {
        folder = Application.persistentDataPath;
        path = folder + Path.DirectorySeparatorChar + GetType().Name;
        Load();
    }

    [MenuIntrospect]
    public void Save() {
        if (!Directory.Exists(folder)) {
            Directory.CreateDirectory(folder);
        }
        File.WriteAllText(path, JsonUtility.ToJson(this));

        Debug.Log(string.Format("{0} saved!", GetType().Name));
    }

    [MenuIntrospect]
    public void Load() {
        try {
            JsonUtility.FromJsonOverwrite(File.ReadAllText(path), this);
            Debug.Log(string.Format("{0} loaded!", GetType().Name));
        } catch (Exception ex) {
            if (ex is FileNotFoundException || ex is DirectoryNotFoundException) {
                Debug.LogWarning(ex);
            } else {
                throw;
            }
        }
    }

}
