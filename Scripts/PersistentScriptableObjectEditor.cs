﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PersistentScriptableObject), true)]
public class PersistentScriptableObjectEditor : Editor {

    public override void OnInspectorGUI() {
        PersistentScriptableObject pso = (PersistentScriptableObject)target;

        DrawDefaultInspector();
        
        if (GUILayout.Button("SAVE")) {
            pso.Save();
        }

        if (GUILayout.Button("LOAD")) {
            pso.Load();
        }

        GUILayout.TextArea(string.Format("{0}\n{1}", "File location:", pso.GetPath()));
    }

}

#endif