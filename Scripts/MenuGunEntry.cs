﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MenuGun {
	public class MenuGunEntry : MonoBehaviour {
		public string customIdentifier;
		public UnityEngine.Object component;
	}
}