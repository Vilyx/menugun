﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;

namespace MenuGun {
    public class Window : Panel {
        public Text text;
        public RectTransform content;
        public RectTransform scrollView;
        public Transform panelsGroup;

        private PanelHeader headerPanel;
        private PanelCall panelCall;

        private ValuePanel[] prefabyValuePanels;
        private ValuePanel[] valuePanels;

        private Panel[] panels;

        private PanelHeader lastHeaderPanel = null;

        public override void SetName(string name) {
            text.text = name;
        }

        public Panel[] GetPanels() {
            return panels;
        }

        public void Link(UnityEngine.Object obj) {
            Identifier = obj.GetType().Name;

            headerPanel = GetComponentInChildren<PanelHeader>();
            panelCall = GetComponentInChildren<PanelCall>();

            prefabyValuePanels = GetComponentsInChildren<ValuePanel>();

            int preCount = panelsGroup.childCount;

            Func<MemberInfo, bool> isIntrospected = x => 
                x.GetCustomAttributes(false).FirstOrDefault(y => y is MenuIntrospectAttribute) != null;

            IEnumerable<FieldInfo> fields = obj.GetType().GetFields()
                .Where(x => (x.DeclaringType == obj.GetType() || isIntrospected(x))
                         && x.IsPublic);
            IEnumerable<PropertyInfo> properties = obj.GetType().GetProperties()
                .Where(x => (x.DeclaringType == obj.GetType() || isIntrospected(x))
                         && x.CanRead
                         && x.CanWrite);
            IEnumerable<MethodInfo> methods = obj.GetType().GetMethods()
                .Where(x => (x.DeclaringType == obj.GetType() || isIntrospected(x))
                         && x.GetParameters().Length == 0
                         && x.ReturnType == typeof(void));
            
            foreach (FieldInfo fi in fields) {
                AddMember(obj, fi);
            }

            foreach (PropertyInfo pi in properties) {
                AddMember(obj, pi);
            }

            foreach (MethodInfo mi in methods) {
                panelCall.Link(obj, mi);
            }

            while (preCount > 0) {
                panelsGroup.GetChild(preCount - 1).gameObject.SetActive(false);
                preCount--;
            }

            valuePanels = GetComponentsInChildren<ValuePanel>();
            panels      = GetComponentsInChildren<Panel>();
        }

        private void AddMember(UnityEngine.Object obj, MemberInfo mi) {
            Type type = null;
            if (mi is FieldInfo) {
                type = (mi as FieldInfo).FieldType;
            } else if (mi is PropertyInfo) {
                type = (mi as PropertyInfo).PropertyType;
            }

            MenuHeaderAttribute ha = mi.GetCustomAttributes(false).FirstOrDefault(x => x is MenuHeaderAttribute) as MenuHeaderAttribute;
            if (ha != null) {
                lastHeaderPanel = headerPanel.Make(ha.header);
            }

            IEnumerable<ValuePanel> can = prefabyValuePanels.Where(x => x.CanHandle(type));
            if (can.Any()) {
                ValuePanel pn = can.First().Link(obj, mi);
                if (lastHeaderPanel != null) {
                    lastHeaderPanel.Add(pn);
                }
            } else {
                Debug.LogWarningFormat("Type {0} is not supported!", mi);
            }
        }

        public void RunFetcher() {
            // this coroutine is running only when winodow is active
            StartCoroutine(Fetcher());
        }

        private IEnumerator Fetcher() {
            while (true) {
                foreach (ValuePanel p in valuePanels) {
                    p.Fetch();
                    yield return null;
                }
                yield return null;
            }
        }

        public void FitContent() {
            if (!LoadWindowState()) {
                scrollView.sizeDelta = new Vector2(scrollView.sizeDelta.x, 
                    panelsGroup.GetComponent<RectTransform>().sizeDelta.y);
            }
        }

        public void ToTop() {
            transform.SetAsLastSibling();
        }

        public void Move(float dx, float dy) {
            transform.Translate(dx, dy, 0);

            SetWindowState();
        }

        public void Resize(float dx, float dy) {
            content.sizeDelta += new Vector2(dx, 0);
            scrollView.sizeDelta -= new Vector2(0, dy);
            ToTop();

            SetWindowState();
        }

        private void SetWindowState() {
            WindowsStateHolder wss = Resources.Load("WindowsStateHolder") as WindowsStateHolder;

            wss.SetWindowState(Identifier, transform.position, new Vector2(content.sizeDelta.x, scrollView.sizeDelta.y));

            // do not save too much
            StopCoroutine("DelayedSave");
            StartCoroutine("DelayedSave");
        }

        private IEnumerator DelayedSave() {
            yield return new WaitForSeconds(0.25f);

            WindowsStateHolder wss = Resources.Load("WindowsStateHolder") as WindowsStateHolder;
            wss.Save();
        }

        private bool LoadWindowState() {
            WindowsStateHolder wss = Resources.Load("WindowsStateHolder") as WindowsStateHolder;

            WindowsStateHolder.WindowState ws = wss.GetWindowState(Identifier);

            if (ws == null) return false;

            transform.position = ws.position;
            content.sizeDelta = new Vector2(ws.size.x, content.sizeDelta.y);
            scrollView.sizeDelta = new Vector2(scrollView.sizeDelta.x, ws.size.y);

            return true;
        }
    }
}