### menugun
Auto-generated in-game inspector for Unity. There is [example project](https://gitlab.com/honix1/menugun-example).

### how to integrate in your project
```
git submodule add https://gitlab.com/Vilyx/menugun.git Assets/menugun
```

### how to use
1. Copy MenuGunCanvas prefab in to your scene.
2. Add some objects to MenuGunCanvas objects array (or use MenuGunEntry component).
3. Press play, windows will appear.

**Ctrl+R** (**Shift+Ctrl+R** in Editor) will place windows right to the mouse position.

### supported types
- int, float
- bool
- string
- Enum
- Vector2
- Vector3
- void() (method call)

### todo
- types:
  - color